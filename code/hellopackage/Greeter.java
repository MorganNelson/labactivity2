package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;
public class Greeter{
    public static void main(String[] args){
        try (java.util.Scanner input = new java.util.Scanner(System.in)) {
            Utilities util = new Utilities();
            int x = util.doubleMe(input.nextInt());
            System.out.println(x);
        }
    }   
}